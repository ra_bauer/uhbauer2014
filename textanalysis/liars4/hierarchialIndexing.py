'''
Dependent on: createDataFrameFromLIWxls.py

This script generates an hierarchial dataframe from the finalDataFrame.pkl, generated in the previous step
This should also be run 6 times.

'''

import pandas as pd
from pandas.core.index import MultiIndex

df = pd.read_pickle("liars4/data/fakeAbstract/finalDataFrame.pkl")

tuplesForHiIndex = []
dictForHiIndex = {}
columns1 = []
for key1, value1 in df.iterrows():
    #print key1
    for key2, value2 in value1.iteritems():
        splitStr = key1.split('###', 1)
        if(key2 not in columns1):
                columns1.append(key2)
        for key3, value3 in value2.iteritems():
            # print splitStr[0], key2, key3, value3
            if splitStr[0] not in dictForHiIndex:
                dictForHiIndex[splitStr[0]] = {key2:{key3:value3}}
            else:
                if(key2 not in dictForHiIndex[splitStr[0]]):
                    dictForHiIndex[splitStr[0]][key2] = {key3:value3}
                else:
                    if(key3 not in dictForHiIndex[splitStr[0]][key2]):
                        dictForHiIndex[splitStr[0]][key2][key3] = value3
                    else:
                        dictForHiIndex[splitStr[0]][key2][key3] += value3
for key , value in dictForHiIndex.iteritems():
    for key1, value1 in value.iteritems():
        for key2, value2 in value1.iteritems():
            if((key, key2)) not in tuplesForHiIndex:
                tuplesForHiIndex.append((key, key2))

index = MultiIndex.from_tuples(tuplesForHiIndex, names=['fileName', 'word'])
df2 = pd.DataFrame(index=index, columns=columns1).fillna(0)

# print df2[df2.index.get_level_values('word') == 'me'].loc['AK 3_2.txt']['Ppron']
df2 = df2.sortlevel(0)

for key, value in dictForHiIndex.iteritems():
    for key1, value1 in value.iteritems():
        for key2, value2 in value1.iteritems():
            # print df2[df2.index.get_level_values('word') == key2].loc[key][key1][key2]
            df2.loc[(key, key2), key1] = value2

print df2.head()
df2.to_pickle("liars4/data/fakeAbstract/hierarchialIndexOfReviews.pkl")
