'''
Starting script:
This script generates 
1. filename_reviews_words.pkl (a dataframe) which contains words and its count for all the reveiws.
    The reivews are divided into 6 categories (real/fake/fakeI/fakeTense/fakeCommon/fakeAbstract)
    Analysis is done individually on all the categories and the results are compared graphically at each step in further scripts.

2. parts_of_speach.pickle (an arraylist) which is a dictionary
    Key: POS
    Value: a dictionary (key: word, value: count)
'''

import xlrd
import pandas as pd
from nltk.tokenize import RegexpTokenizer, word_tokenize
import nltk
from nltk.stem.snowball import PorterStemmer
import enchant
from collections import Counter
import cPickle as pickle
from enum import Enum

stemmer = PorterStemmer()
stop = nltk.corpus.stopwords.words('english')
tokenizer = RegexpTokenizer(r'\w+')

def removeNonAsciiChars(line):
    return ''.join([i if ord(i) < 128 else '' for i in line])

def stemWords(line):
    return " ".join(stemmer.stem(word) for word in line.split())

def removeExclamations(line):
    return " ".join(tokenizer.tokenize(line))

def removeStopWords(line):
    return " ".join(word for word in line.split() if word not in stop)

def createTermDocumentMatrix(d, key, value, reviewType):
    columnsinDF1 = []
    rowsInDF1 = []
    for key2, value2 in value.iteritems():  # print key2, value2
        rowsInDF1.append(key + "###" + key2)
        for word2 in removeExclamations(stemWords(removeStopWords(removeNonAsciiChars(value2)))).split():
            if d.check(word2):
                if word2 not in columnsinDF1:
                    columnsinDF1.append(word2)
    
    df2 = pd.DataFrame(index=rowsInDF1, columns=columnsinDF1)
    for key2, value2 in value.iteritems():
        for tuple in Counter(value2.split()).most_common():
            df2.loc[key + "###" + key2][tuple[0]] = tuple[1]
    
    df2 = df2.fillna(0)
    df2.to_pickle("liars4/data/" + reviewType + "/" + key + ".pkl")
    print df2.head()
    
dict1 = {}
d = enchant.Dict("en_US")

columnsInDF = {}
indexInDF = {}
partsOFspeach = {}

path = "liars4/resource/Liars4_for_analysis.xlsx"
book = xlrd.open_workbook(path)
worksheet = book.sheet_by_index(0)
num_rows = worksheet.nrows - 1
num_cells = worksheet.ncols - 1
curr_row = -1
counter = 1

while curr_row < num_rows:
        curr_row += 1
        cell_review_type = worksheet.cell_value(curr_row, 1)
        cell_review = worksheet.cell_value(curr_row, 2)
        # print cell_review_type, cell_review
        review = cell_review.strip()
        for word in nltk.pos_tag(word_tokenize(removeExclamations(stemWords(removeStopWords(removeNonAsciiChars(review)))))):
            if cell_review_type in partsOFspeach:
                partsOFspeach.get(cell_review_type).append(word)
            else:
                partsOFspeach[cell_review_type] = [word]
            if d.check(word[0]):
                if cell_review_type in columnsInDF:
                    if word[0] not in columnsInDF.get(cell_review_type):
                        columnsInDF.get(cell_review_type).append(word[0])
                else:
                    columnsInDF[cell_review_type] = [word[0]]
                    
        if review != "":
            if cell_review_type in indexInDF:
                indexInDF.get(cell_review_type).append("ABCDE###_" + str(counter))
                dict1[cell_review_type]["ABCDE###_" + str(counter)] = review
            else:
                indexInDF[cell_review_type] = ["ABCDE###_" + str(counter)]
                dict1[cell_review_type] = {"ABCDE###_" + str(counter): review}
            counter = counter + 1
# print partsOFspeach.get(1.0)
# print indexInDF.get(1.0)
# print columnsInDF.get(1.0)

dfs = {}
for key, value in indexInDF.iteritems():
    dfs[key] = pd.DataFrame(index=value, columns=columnsInDF.get(key))

# print dfs.get(1.0)

# parts of speech tagging
for key, value in partsOFspeach.iteritems():
    if key == 1.0:
        pickle.dump(value, open("liars4/data/real/parts_of_speach.pickle", "wb"))
    if key == 2.0:
        pickle.dump(value, open("liars4/data/fake/parts_of_speach.pickle", "wb"))
    if key == 3.0:
        pickle.dump(value, open("liars4/data/fakeI/parts_of_speach.pickle", "wb"))
    if key == 4.0:
        pickle.dump(value, open("liars4/data/fakeTense/parts_of_speach.pickle", "wb"))
    if key == 5.0:
        pickle.dump(value, open("liars4/data/fakeCommon/parts_of_speach.pickle", "wb"))
    if key == 6.0:
        pickle.dump(value, open("liars4/data/fakeAbstract/parts_of_speach.pickle", "wb"))
    
    

for key, value in dfs.iteritems():
    for key1 , value1 in dict1.get(key).iteritems():
        value.loc[key1]['ABCDE'] = value1
        for tuple in Counter(value1.split()).most_common():
            value.loc[key1][tuple[0]] = tuple[1]
    value = value.fillna(0)
    # print value.head()
    if key == 1.0:
        value.to_pickle("liars4/data/real/filename_reviews_words.pkl")
    if key == 2.0:
        value.to_pickle("liars4/data/fake/filename_reviews_words.pkl")
    if key == 3.0:
        value.to_pickle("liars4/data/fakeI/filename_reviews_words.pkl")
    if key == 4.0:
        value.to_pickle("liars4/data/fakeTense/filename_reviews_words.pkl")
    if key == 5.0:
        value.to_pickle("liars4/data/fakeCommon/filename_reviews_words.pkl")
    if key == 6.0:
        value.to_pickle("liars4/data/fakeAbstract/filename_reviews_words.pkl")
    
        
dict2 = {}
for key , value in dict1.iteritems():
    for key1, value1 in value.iteritems():
        splitStr = key1.split('###', 1)
        if key in dict2:
            if splitStr[0] not in dict2.get(key):
                dict2[key][splitStr[0]] = {splitStr[1]:value1}
            else:
                dict2[key][splitStr[0]][splitStr[1]] = value1
        else:
            dict2[key] = {splitStr[0]:{splitStr[1]:value1}}

# print dict2.get(1.0)

for key, value in dict2.iteritems():
    for key1, value1 in value.iteritems():
        if key == 1.0:
            createTermDocumentMatrix(d, key1, value1, "real")
        if key == 2.0:
            createTermDocumentMatrix(d, key1, value1, "fake")
        if key == 3.0:
            createTermDocumentMatrix(d, key1, value1, "fakeI")
        if key == 4.0:
            createTermDocumentMatrix(d, key1, value1, "fakeTense")
        if key == 5.0:
            createTermDocumentMatrix(d, key1, value1, "fakeCommon")
        if key == 6.0:
            createTermDocumentMatrix(d, key1, value1, "fakeAbstract")




