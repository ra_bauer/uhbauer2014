'''
Dependent on: hierarchialIndexing.py

This script divides the 6 dataframes generated from the previous step into
64 dataframes (LWIC).
Each dataframe contains data for particular LWIC.

'''

import pandas as pd
from pandas.core.frame import DataFrame

def getcolumns(dict1, columns):
    for key, value in dict1.iteritems():
        for key1, value1 in value.iteritems():
            for key2, value2 in value1.iteritems():
                if key1 not in columns:
                    columns[key1] = [key2]
                else:
                    if key2 not in columns[key1]:
                        columns[key1].append(key2)
    
    return columns

def getDict(df):
    dict1 = {}
    for key1, value1 in df.iterrows():
        for key2, value2 in value1.iteritems():
        # print key1, key2, value2
            if key1[0] not in dict1:
                dict1[key1[0]] = {key2:{key1[1]:value2}}
            elif key2 not in dict1[key1[0]]:
                dict1[key1[0]][key2] = {key1[1]:value2}
            else:
                dict1[key1[0]][key2][key1[1]] = value2
    
    return dict1

df = pd.read_pickle("liars4/data/real/hierarchialIndexOfReviews.pkl")
df1 = pd.read_pickle("liars4/data/fake/hierarchialIndexOfReviews.pkl")
df2 = pd.read_pickle("liars4/data/fakeI/hierarchialIndexOfReviews.pkl")
df3 = pd.read_pickle("liars4/data/fakeTense/hierarchialIndexOfReviews.pkl")
df4 = pd.read_pickle("liars4/data/fakeCommon/hierarchialIndexOfReviews.pkl")
df5 = pd.read_pickle("liars4/data/fakeAbstract/hierarchialIndexOfReviews.pkl")
# print df.head()
# print df1.head()

dict1 = getDict(df)
dict2 = getDict(df1)  
dict3 = getDict(df2)
dict4 = getDict(df3)  
dict5 = getDict(df4)
dict6 = getDict(df5)  
    

# print dict1
columns = {}
columns = getcolumns(dict1, columns)
columns = getcolumns(dict2, columns)
columns = getcolumns(dict3, columns)
columns = getcolumns(dict4, columns)
columns = getcolumns(dict5, columns)
columns = getcolumns(dict6, columns)

# print columns
dataFrames = {}
for key, value in dict1.iteritems():
    for key1, value1 in value.iteritems():
        dataFrames[key1] = df2 = pd.DataFrame(index=columns.get(key1), columns=['Real', 'Fake', 'FakeI', 'FakeTense', 'FakeCommon', 'FakeAbstract']).fillna(0)

for key, value in {"FakeI":dict3, "FakeTense":dict4, "FakeCommon":dict5, "FakeAbstract":dict6, "Real":dict2, "Fake":dict1}.iteritems():
    for key1, value1 in value.iteritems():
        for key2, value2 in value1.iteritems():
            for key3, value3 in value2.iteritems():
                # print key3, value3
                dataFrames.get(key2).loc[key3][key] = value3

for key , value in dataFrames.iteritems():
    # print value
    value.to_pickle('liars4/data/wordCatgry_wordVsCount/' + key + '_wordcat.pkl')
