Analysis of liar4 data in liars4/resources

The scripts are interdependent and should be run in an order

1. createReviewsDataFromXls.py
2. createDataFrameFromLIWxls.py
3. hierarchialIndexing.py
4. hierarchialIndexOfReviews.py
5. partsOfSpeechTagging.py
6. plotSingleWordCatBars.py