'''
Dependent on: createReviewsDataFromXls.py

This scripts plots multi-bar graph of all the POS.
Xaxis: POS
YAxis: count of POS

'''

import cPickle as pickle
from nltk.tag import map_tag
import pandas as pd
import numpy as np
import operator


def plotBars(df3, filename):
    ax = df3.plot(kind='bar', fontsize=5)
    for p in ax.patches:
        ax.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005), fontsize=5)
    
    fig = ax.get_figure()
    fig.savefig('liars4/plot/' + filename)
    print "Saved " + filename

frech_universal_co = {"NOUN":['NN', 'NNS', 'PRP', 'WP'], "VERB":['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ', 'MD', 'TO'],
                      "PRON":['NNP', 'NNPS'], "ADJ":['JJ', 'JJR', 'JJS'], "ADV":['RB', 'RBR', 'RBS'], "DET":['DT', 'WDT'],
                      "ADP":['IN'], "NUM":[], "CONJ":['CC'], "PRT":['RP']}

def getSorting(stats):
    return max(stats.iteritems(), key=operator.itemgetter(1))[0] 

def getDict(path):
    posTagged = pickle.load(open(path, "rb"))
    simplifiedTagged = [(word, map_tag('en-ptb', 'universal', tag)) for word, tag in posTagged]  # print simplifiedTagged
    dict1 = {}
    for word in posTagged:
        if word[1] in dict1:
            dict1[word[1]] = dict1[word[1]] + 1
        else:
            dict1[word[1]] = 1
    return dict1

realDict = getDict("liars4/data/real/parts_of_speach.pickle")
fakeDict = getDict("liars4/data/fake/parts_of_speach.pickle")
fakeIDict = getDict("liars4/data/fakeI/parts_of_speach.pickle")
fakeTenseDict = getDict("liars4/data/fakeTense/parts_of_speach.pickle")
fakeCommonDict = getDict("liars4/data/fake/parts_of_speach.pickle")
fakeAbstractDict = getDict("liars4/data/fake/parts_of_speach.pickle")

df1 = pd.DataFrame(index=realDict.keys(), columns=['Real', 'Fake', 'FakeI', 'FakeTense', 'FakeCommon', 'FakeAbstract']).fillna(0)

for key, value in realDict.iteritems():
    df1['Real'].loc[key] = value
    
for key, value in fakeDict.iteritems():
    df1['Fake'].loc[key] = value

for key, value in fakeIDict.iteritems():
    df1['FakeI'].loc[key] = value
    
for key, value in fakeTenseDict.iteritems():
    df1['FakeTense'].loc[key] = value

for key, value in fakeCommonDict.iteritems():
    df1['FakeCommon'].loc[key] = value
    
for key, value in fakeAbstractDict.iteritems():
    df1['FakeAbstract'].loc[key] = value
    
# print df1
sorting = []
df2 = np.round(df1.apply(lambda c: c / c.sum() * 100, axis=0), 2)
sorting = [getSorting({'Real':df2['Real'].max(), 'FakeI':df2['FakeI'].max(), 'FakeTense':df2['FakeTense'].max(), 'FakeCommon':df2['FakeCommon'].max(), 'FakeAbstract':df2['FakeAbstract'].max(), 'Fake':df2['Fake'].max() })]
df3 = df2.sort(columns=sorting, ascending=False)
df3.to_pickle("liars4/data/parstOfSpeachTagging.pkl")
plotBars(df3[:9], "posPlot1.pdf")
plotBars(df3[9:18], "posPlot2.pdf")
plotBars(df3[18:27], "posPlot3.pdf")
