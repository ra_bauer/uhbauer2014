'''
Dependent on: createDataFrameFromLIWxls.py

This script generates
finalDataFrame.pkl (a dataframe)
Previous data filename_reviews_words.pkl is further divided into 64 LIW categories and stored in a dataframe.
This script should be run 6 times after changing the appropriate paths in the code.
Sicne there are six similar files (filename_reviews_words.pkl) in following folders.
real/
fake/
fakeI/
fakeTense/
fakeCommon/
fakeAbstract/

'''
import xlrd
import os
import pandas as pd

def write_excel_into_dict(path):
    dict1 = {}
    keys = {}
    book = xlrd.open_workbook(path)
    worksheet = book.sheet_by_index(0)
    num_rows = worksheet.nrows - 1
    num_cells = worksheet.ncols - 1
    curr_row = -1
    while curr_row < num_rows:
        curr_row += 1
        # print 'Row:', curr_row
        curr_cell = -1
        while curr_cell < num_cells:
            curr_cell += 1
            # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
            cell_review_type = worksheet.cell_type(curr_row, curr_cell)
            cell_value = worksheet.cell_value(curr_row, curr_cell)
            # print '    ', curr_cell , cell_review_type, ':', cell_value
            if(curr_row == 2 and cell_review_type == 1):
                keys.update({curr_cell:cell_value})
                dict1.update({cell_value:[]})
            if(curr_row > 2 and cell_review_type == 1):
                dict1.get(keys.get(curr_cell)).append(cell_value)
    return dict1

path = "resources/LIWC2007dictionary_poster.xls"
dict1 = write_excel_into_dict(path)
# print dict1

indexInDF = []
columnsInDF = dict1.keys()

df1 = pd.read_pickle("liars4/data/fakeAbstract/filename_reviews_words.pkl")
for key, value in df1.iterrows():
    indexInDF.append(key)

data = []
for i in range(len(indexInDF)):
        data.append([])
        for j in range(len(columnsInDF)):
                data[i].append({})

df2 = pd.DataFrame(data, index=indexInDF, columns=columnsInDF)

path1 = "liars4/data/fakeAbstract";
for file1 in os.listdir(path1):
    if file1.endswith(".pkl") and "ABCDE" in file1:
        # print path1 + '/' + file1
        df = pd.read_pickle(path1 + '/' + file1)
        for key1, value1 in df.iterrows():
            for key2, value2 in value1.iteritems ():
                # print key2,value2
                for key3, value3 in dict1.iteritems():
                    if key2 in value3:
                            df2.loc[key1][key3][key2] = value2
        print "completed for file: " + file1
print df2.head()
df2.to_pickle("liars4/data/fakeAbstract/finalDataFrame.pkl")
