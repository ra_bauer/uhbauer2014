import xlrd
import os
from enum import Enum
import random
import pandas as pd
from nltk.tokenize import RegexpTokenizer
import nltk
from nltk.stem.snowball import SnowballStemmer
import enchant
from collections import Counter

stemmer = SnowballStemmer("english")
stop = nltk.corpus.stopwords.words('english')
tokenizer = RegexpTokenizer(r'\w+')

def removeNonAsciiChars(line):
    return ''.join([i if ord(i) < 128 else '' for i in line])

def stemWords(line):
    return " ".join(stemmer.stem(word) for word in line.split())

def removeExclamations(line):
    return " ".join(tokenizer.tokenize(line))

def removeStopWords(line):
    return " ".join(word for word in line.split() if word not in stop)

def createTermDocumentMatrix(d, key, value):
    columnsinDF1 = []
    rowsInDF1 = []
    for key2, value2 in value.iteritems():  # print key2, value2
        rowsInDF1.append(key + "###" + key2)
        for word2 in removeExclamations(stemWords(removeStopWords(removeNonAsciiChars(value2)))).split():
            if d.check(word2):
                if word2 not in columnsinDF1:
                    columnsinDF1.append(word2)
    
    df2 = pd.DataFrame(index=rowsInDF1, columns=columnsinDF1)
    for key2, value2 in value.iteritems():
        for tuple in Counter(value2.split()).most_common():
            df2.loc[key + "###" + key2][tuple[0]] = tuple[1]
    
    df2 = df2.fillna(0)
    df2.to_pickle("dataframes/reviews2/" + key + ".pkl")

dict1 = {}
d = enchant.Dict("en_US")
columns1 = []
columnsInDF = []
indexInDF = []
listOfFiles = []

path = "resources/reviews2/Dish Soap Combined Real-Fake.xlsx"
book = xlrd.open_workbook(path)
worksheet = book.sheet_by_index(1)
num_rows = worksheet.nrows - 1
num_cells = worksheet.ncols - 1
curr_row = -1
counter = 1
while curr_row < num_rows:
        curr_row += 1
        # print 'Row:', curr_row
        curr_cell = 1
        cell_review_type = worksheet.cell_type(curr_row, curr_cell)
        cell_value = worksheet.cell_value(curr_row, curr_cell)
        #print cell_value
        review = cell_value.strip()
        for word in removeExclamations(stemWords(removeStopWords(removeNonAsciiChars(review)))).split():
            if d.check(word):
                    if word not in columnsInDF:
                        columnsInDF.append(word)
        if review != "":
                indexInDF.append("review_" + str(counter)+"###1")
                dict1["review_" + str(counter)+"###1"] = review
                counter = counter + 1

#print dict1
df = pd.DataFrame(index=indexInDF, columns=columnsInDF)

for key , value in dict1.iteritems():
    df.loc[key]['review'] = value
    for tuple in Counter(value.split()).most_common():
        df.loc[key][tuple[0]] = tuple[1]

df = df.fillna(0)
#print df.head()
df.to_pickle("dataframes/reviews2/filename_reviews_words.pkl")

print df.head()

dict2 = {}
for key , value in dict1.iteritems():
    splitStr = key.split('###', 1)
    if splitStr[0] not in dict2:
        dict2[splitStr[0]] = {splitStr[1]:value}
    else:
        dict2[splitStr[0]][splitStr[1]] = value
        
for key, value in dict2.iteritems():
    createTermDocumentMatrix(d, key, value)