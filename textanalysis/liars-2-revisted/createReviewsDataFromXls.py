import xlrd
import pandas as pd
from nltk.tokenize import RegexpTokenizer, word_tokenize
import nltk
from nltk.stem.snowball import PorterStemmer
import enchant
from collections import Counter
import cPickle as pickle

stemmer = PorterStemmer()
stop = nltk.corpus.stopwords.words('english')
tokenizer = RegexpTokenizer(r'\w+')

def removeNonAsciiChars(line):
    return ''.join([i if ord(i) < 128 else '' for i in line])

def stemWords(line):
    return " ".join(stemmer.stem(word) for word in line.split())

def removeExclamations(line):
    return " ".join(tokenizer.tokenize(line))

def removeStopWords(line):
    return " ".join(word for word in line.split() if word not in stop)

def createTermDocumentMatrix(d, key, value):
    columnsinDF1 = []
    rowsInDF1 = []
    for key2, value2 in value.iteritems():  # print key2, value2
        rowsInDF1.append(key + "###" + key2)
        for word2 in removeExclamations(stemWords(removeStopWords(removeNonAsciiChars(value2)))).split():
            if d.check(word2):
                if word2 not in columnsinDF1:
                    columnsinDF1.append(word2)
    
    df2 = pd.DataFrame(index=rowsInDF1, columns=columnsinDF1)
    for key2, value2 in value.iteritems():
        for tuple in Counter(value2.split()).most_common():
            df2.loc[key + "###" + key2][tuple[0]] = tuple[1]
    
    df2 = df2.fillna(0)
    df2.to_pickle("dataframes/" + key + ".pkl")
    # print df2.head()
    
dict1 = {}
d = enchant.Dict("en_US")
columns1 = []
columnsInDF = []
indexInDF = []
listOfFiles = []
partsOFspeach = []
path = "resources/Liars-2-revised-liquid-soap-reviews.xlsx"
book = xlrd.open_workbook(path)
worksheet = book.sheet_by_index(0)
num_rows = worksheet.nrows - 1
num_cells = worksheet.ncols - 1
curr_row = -1
counter = 1
while curr_row < num_rows:
        curr_row += 1
        # print 'Row:', curr_row
        curr_cell = 1
        cell_type = worksheet.cell_type(curr_row, curr_cell)
        cell_value = worksheet.cell_value(curr_row, curr_cell)
        # print cell_value
        review = cell_value.strip()
        for word in nltk.pos_tag(word_tokenize(removeExclamations(stemWords(removeStopWords(removeNonAsciiChars(review)))))):
            partsOFspeach.append(word)
            if d.check(word[0]):
                    if word[0] not in columnsInDF:
                        columnsInDF.append(word[0])
        if review != "":
                indexInDF.append("review###_" + str(counter))
                dict1["review###_" + str(counter)] = review
                counter = counter + 1

df = pd.DataFrame(index=indexInDF, columns=columnsInDF)

for key , value in dict1.iteritems():
    df.loc[key]['review'] = value
    for tuple in Counter(value.split()).most_common():
        df.loc[key][tuple[0]] = tuple[1]

df = df.fillna(0)
# print df.head()
df.to_pickle("dataframes/reviews_words_real.pkl")

print df.head()

pickle.dump(partsOFspeach, open("dataframes/reviews_words_real_pos.pickle", "wb"))

print "-----------------------------------------------------------------------------"

'''
dict2 = {}
for key , value in dict1.iteritems():
    splitStr = key.split('###', 1)
    if splitStr[0] not in dict2:
        dict2[splitStr[0]] = {splitStr[1]:value}
    else:
        dict2[splitStr[0]][splitStr[1]] = value
        
for key, value in dict2.iteritems():
    createTermDocumentMatrix(d, key, value)
'''