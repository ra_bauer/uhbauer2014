import nltk
import string
import os
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer
import xlrd
import pandas as pd
from nltk.tokenize import RegexpTokenizer

stemmer = PorterStemmer()
tokenizer = RegexpTokenizer(r'\w+')
stop = nltk.corpus.stopwords.words('english')

path = "resources/Liars-2-revised-liquid-soap-reviews.xlsx"
token_dict = {}

def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = stem_tokens(tokens, stemmer)
    return stems

def removeNonAsciiChars(line):
    return ''.join([i if ord(i) < 128 else '' for i in line])

def stemWords(line):
    return " ".join(stemmer.stem(word) for word in line.split())

def removeExclamations(line):
    return " ".join(tokenizer.tokenize(line))

def removeStopWords(line):
    return " ".join(word for word in line.split() if word not in stop)


book = xlrd.open_workbook(path)
worksheet = book.sheet_by_index(1)
num_rows = worksheet.nrows - 1
num_cells = worksheet.ncols - 1
curr_row = -1
counter = 1
while curr_row < num_rows:
        curr_row += 1
        # print 'Row:', curr_row
        curr_cell = 1
        cell_type = worksheet.cell_type(curr_row, curr_cell)
        cell_value = worksheet.cell_value(curr_row, curr_cell)
        # print cell_value
        review = cell_value.strip()
        review = removeExclamations(stemWords(removeStopWords(removeNonAsciiChars(review))))
        if review != "":
                token_dict["review###_" + str(counter)] = review
                counter = counter + 1

tfidf = TfidfVectorizer(tokenizer=tokenize, stop_words='english')
tfs = tfidf.fit_transform(token_dict.values())
feature_names = tfidf.get_feature_names()

dict1 = {}

for col in tfs.nonzero()[1]:
    # print feature_names[col], ' - ', tfs[0, col]
    dict1[feature_names[col]] = tfs[0, col]

df1 = pd.DataFrame(index=dict1.keys(), columns=['score'])

for key , value in dict1.iteritems():
    df1.loc[key] = value

df1.to_pickle("dataframes/TFIDF_fake.pkl")

print df1