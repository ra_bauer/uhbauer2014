from nltk.tokenize.punkt import PunktSentenceTokenizer, PunktParameters
import xlrd
import pandas as pd
from collections import OrderedDict
from pandas.core.index import MultiIndex

punkt_param = PunktParameters()
punkt_param.abbrev_types = set(['dr', 'vs', 'mr', 'mrs', 'prof', 'inc'])
sentence_splitter = PunktSentenceTokenizer(punkt_param)

tuplesForHiIndex = []
dictForHiIndex = OrderedDict()
path = "resources/Liars-2-revised-liquid-soap-reviews.xlsx"
book = xlrd.open_workbook(path)
worksheet = book.sheet_by_index(1)
num_rows = worksheet.nrows - 1
num_cells = worksheet.ncols - 1
curr_row = -1
counter = 1
while curr_row < num_rows:
        curr_row += 1
        # print 'Row:', curr_row
        curr_cell = 1
        cell_type = worksheet.cell_type(curr_row, curr_cell)
        cell_value = worksheet.cell_value(curr_row, curr_cell)
        # print cell_value
        review = cell_value.strip()
        review = review.replace('?"', '? "').replace('!"', '! "').replace('."', '. "')
        if review != "":
            sentences = sentence_splitter.tokenize(review)
            counter1 = 1
            dict1 = OrderedDict()
            for sentence in sentences:
                dict1.update({"sentence#" + str(counter1):sentence})
                counter1 = counter1 + 1
            dictForHiIndex["review#" + str(counter)] = dict1                
            counter = counter + 1

columns1 = []
for key, value in dictForHiIndex.iteritems():
    for key1, value1 in value.iteritems():
        #print key , key1 , value1
        tuplesForHiIndex.append((key, key1))
        columns1.append(value1)
        
index = MultiIndex.from_tuples(tuplesForHiIndex, names=['reviewNum', 'sentenceNum'])
df2 = pd.DataFrame(columns1 , index=index, columns=['sentence']).fillna(0)

df2.to_pickle("dataframes/sentences_fake.pkl")
