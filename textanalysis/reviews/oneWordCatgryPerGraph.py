import pandas as pd
import matplotlib.pyplot as plt

df2 = pd.read_pickle("dataframes/reviews/hierarchialIndexOfReviews.pkl")
df1 = df2.groupby(level='fileName').sum()

def plotBarGraph(xaxis, yaxis, title, xlabel, ylabel):
    plt.figure()
    barlist = plt.bar(range(1, xaxis.__len__() + 1), yaxis, align='center')
    barlist[1].set_color('r')
    barlist[3].set_color('g')
    barlist[5].set_color('g')
    barlist[7].set_color('r')
    plt.xticks(range(1, xaxis.__len__() + 1), xaxis, size='small')
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig('plots/wordCatgryPlots/' + title + '.pdf')

dict1 = {}

for index in df1.index.values:
    for key , value in df1.loc[index].iteritems():
        # print index,key,value
        if key not in dict1:
            dict1[key] = {'xaxis':[index], 'yaxis':[value]}
        else:
            dict1[key]['xaxis'].append(index)
            dict1[key]['yaxis'].append(value)

for key, value in dict1.iteritems():
    print key , value['xaxis'], value['yaxis']
    plotBarGraph(value['xaxis'], value['yaxis'], key, 'document', 'word count')
    # plotBars(value['xaxis'], value['yaxis'], key)
    
