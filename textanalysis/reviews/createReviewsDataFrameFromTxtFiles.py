import os
from enum import Enum
import random
import pandas as pd
from nltk.tokenize import RegexpTokenizer
import nltk
from nltk.stem.snowball import SnowballStemmer
import enchant
from collections import Counter

stemmer = SnowballStemmer("english")
stop = nltk.corpus.stopwords.words('english')
tokenizer = RegexpTokenizer(r'\w+')

class Foo(Enum):
    test_condition_1 = "condition1"
    test_condition_2 = "condition2"
    test_condition_3 = "condition3"
    test_condition_4 = "condition4"
    test_condition_5 = "condition5"

def removeNonAsciiChars(line):
    return ''.join([i if ord(i) < 128 else '' for i in line])

def stemWords(line):
    return " ".join(stemmer.stem(word) for word in line.split())

def removeExclamations(line):
    return " ".join(tokenizer.tokenize(line))

def removeStopWords(line):
    return " ".join(word for word in line.split() if word not in stop)

path = "resources";
dict = {}
columns1 = []
columnsInDF = []
listOfFiles = []
d = enchant.Dict("en_US")

for file in os.listdir(path):
    if file.endswith(".txt"):
        print 'reading file: ' + path + '/' + file
        listOfFiles.append(file)
        f = open(path + '/' + file, 'r+')
        for line in f:
            review = line.strip()
            for word in removeExclamations(stemWords(removeStopWords(removeNonAsciiChars(review)))).split():
                if d.check(word):
                    if word not in columnsInDF:
                        columnsInDF.append(word)
            if review != "":
                condition = random.choice(list(Foo)).value
                if file not in dict:
                    dict[file] = {condition : [review]}
                else:
                    if condition not in dict[file]:
                        dict[file][condition] = [review]
                    else:
                        dict[file][condition].append(review)
        print "done..."
# docId = []
# condition = []
# review12 = []
# count = 1        

# for key, value in dict.iteritems():
#    for key1, value1 in value.iteritems():
#        condition.append(key1)
#        review12.append(value1)
#        docId.append(key)
#        count = count + 1

# df = pd.DataFrame({'id':range(1, count) , 'docId':docId, "condition":condition, "review":review12}, columns=['id', 'docId', 'condition', 'review']) 
# print df.head()

# for key, value in dict.iteritems():
#    print key
#    for key1, value1 in value.iteritems():
#        print "---", key1
    
df = pd.DataFrame.from_dict(dict, orient='index')
df.to_pickle("dataframes/reviews/filename_reviews.pkl")
# print df.head()

d = enchant.Dict("en_US")
dict2 = {}
for key, value in dict.iteritems():
    dict3 = {}
    for key1, value1 in value.iteritems():
        dict1 = {}
        count = 1
        for review1 in value1:
            arr = []
            for tuple in Counter(review1.split()).most_common():
                arr.append(tuple)
            dict1.update({count : arr})
            count = count + 1
        dict3.update({key1:dict1})
    dict2.update({key:dict3})

print "###########"

# for key, value in dict2.iteritems():
    # print key
#    for key1, value1 in value.iteritems():
#        print "---", value1

df2 = pd.DataFrame.from_dict(dict2, orient='index')
df2.to_pickle("dataframes/reviews/filename_reviews_corupus.pkl")
print df2.head()
