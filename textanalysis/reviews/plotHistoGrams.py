import pandas as pd
import matplotlib.pyplot as plt


def main():
    df2 = pd.read_pickle("dataframes/reviews/hierarchialIndexOfReviews.pkl")
    df1 = df2.groupby(level='fileName').sum()
    df3 = df2.sum(axis=1)
    plotWordCatogery(df1)
    plotIndividualWords(df3)

def plotWordCatogery(df1):
    for index in df1.index.values:
        xaxis = []
        yaxis = []
        for key, value in df1.loc[index].iteritems():
            xaxis.append(key)
            yaxis.append(value)
        plotBarGraph(xaxis, yaxis, index + '_wordCategory', "word category", "word count")

def plotIndividualWords(df3):
    dict1 = {}
    for index in df3.index.values:
        if index[0] not in dict1:
            dict1[index[0]] = {'xaxis':[index[1]], 'yaxis':[df3.loc[index]]}
        else:
            dict1[index[0]]['xaxis'].append(index[1])
            # print index[0], df3.loc[index]
            dict1[index[0]]['yaxis'].append(df3.loc[index])
    
    for key2, value2 in dict1.iteritems():
        plotBarGraph(value2['xaxis'], value2['yaxis'], key2 + '_indvWords', "individual words", "word count")

def plotBarGraph(xaxis, yaxis, title, xlabel, ylabel):
    plt.bar(range(1, xaxis.__len__() + 1), yaxis, align='center')
    plt.xticks(range(1, xaxis.__len__() + 1), xaxis)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig('plots/' + title + '.pdf')

main()
