import pandas as pd
import plotly.plotly as py
from plotly.graph_objs import *
# Fill in with your personal username and API key
# or, use this public demo account

def plotOverlappingHistograms(traces):
    py.sign_in('ravikiran0215', 'l0d4eme4ju')
    data = Data(traces)
    layout = Layout(
        barmode='group'
    ) 
    fig = Figure(data=data, layout=layout)
    plot_url = py.plot(fig, filename='../plots/overlappingHistogram.pdf')
    
def getTrace(x1, y1, name1):
    return Bar(x=x1, y=y1, name=name1)

def plotWordCatogery(df1):
    traces = []
    for index in df1.index.values:
        xaxis = []
        yaxis = []
        for key, value in df1.loc[index].iteritems():
            xaxis.append(key)
            yaxis.append(value)
        traces.append(getTrace(xaxis,yaxis,index))
    plotOverlappingHistograms(traces)   
        
        
df2 = pd.read_pickle("../dataframes/reviews/hierarchialIndexOfReviews.pkl")
df1 = df2.groupby(level='fileName').sum()
df3 = df2.sum(axis=1)
plotWordCatogery(df1)