import os
import pandas as pd
import numpy as np

dataFrames = {}
path1 = "dataframes/reviews3/";
for file1 in os.listdir(path1 + 'wordCatgry_wordVsCount/'):
    if file1.endswith(".pkl") and "wordcat" in file1:
        df = pd.read_pickle(path1 + '/wordCatgry_wordVsCount/' + file1)
        dataFrames[file1] = df
# print dataFrames

for key, value in dataFrames.iteritems():
    sorting = []
    df1 = np.round(value.apply(lambda c: c / c.sum() * 100, axis=0), 2)
    if df1['Real'].max() > df1['Fake'].max():
        sorting = ['Real']
    else:
        sorting = ['Fake']
    df1.sort(columns=sorting, ascending=False).to_pickle(path1 + 'wordCatgry_wordVsPercentage/' + key)
    df2 = df1.sort(columns=sorting, ascending=False).head(40) 
    #print key, df1['Real'].max(), df1['Fake'].max(), df2
    ax = df2.plot(kind='bar', fontsize=5)
    for p in ax.patches:
        ax.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005), fontsize=5)
    fig = ax.get_figure()
    fig.savefig('plots/reviews3/wordCatGraphs/' + key + '.pdf')
    print "Saved ", key + ".pdf"
