import pandas as pd


def getDicts(df2):
    dict4 = {}
    for index in df2.index.values:
    # print index, df2.loc[index]
        for key, value in df2.loc[index].iteritems():
        # print index[1], key, value
            if key not in dict4:
                dict4[key] = {index[1]:value}
            else:
                dict4[key][index[1]] = value
    
    return dict4

def getDict(path):
    df2 = pd.read_pickle(path)  # print df2.head()
    df1 = df2.groupby(level='fileName').sum()  
    # print df1.head()
    dict1 = {}
    for index in df1.index.values:
        for key, value in df1.loc[index].iteritems():
            # print index, key, value
            if index not in dict1:
                dict1[index] = {'xaxis':[key], 'yaxis':[value]}
            else:
                dict1[index]['xaxis'].append(key)
                dict1[index]['yaxis'].append(value)
    
    return dict1, df2

def plotOverlappingGraph(dict1, title):
    print dict1
    dict2 = {}
    for key, value in dict1.iteritems():
        dict2[key] = value['yaxis']
    # print dict2
    df4 = pd.DataFrame(dict2, index=value['xaxis']) 
    print df4.head()
    ax = df4.plot(kind='bar', fontsize=4)
    for p in ax.patches:
        ax.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005), fontsize=3)
    fig = ax.get_figure()
    fig.savefig('plots/reviews2Revisit/' + title + '.pdf')
            
dict1, df2 = getDict("dataframes/reviews2Revisit/fake/hierarchialIndexOfReviews.pkl")
dict2, df3 = getDict("dataframes/reviews2Revisit/real/hierarchialIndexOfReviews.pkl")            

dict3 = {}
for key, value in {"Real":dict2, "Fake":dict1}.iteritems():
    for key1, value1 in value.iteritems():
        if key1 not in dict3:
            dict3[key1] = {key:value1}
        else:
            dict3[key1][key] = value1

# print df2

dict5 = getDicts(df2)
dict6 = getDicts(df3)

print dict5

dict7 = {}
for key , value in {"Real":dict6, "Fake":dict5}.iteritems():
    for key1, value1 in value.iteritems():
        # print key,key1
        if key1 not in dict7:
            dict7[key1] = {key:value1}
        else:
            dict7[key1][key] = value1

# print dict7

for key , value in dict7.iteritems():
    for key1, value1 in value.iteritems():
        #print key , key1, value1
        pass
    
# for key , value in dict3.iteritems():
#    plotOverlappingGraph(value, key)
