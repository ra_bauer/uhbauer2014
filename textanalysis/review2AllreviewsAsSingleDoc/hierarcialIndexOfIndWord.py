import pandas as pd
from pandas.core.frame import DataFrame

def getcolumns(dict1, columns):
    for key, value in dict1.iteritems():
        for key1, value1 in value.iteritems():
            for key2, value2 in value1.iteritems():
                if key1 not in columns:
                    columns[key1] = [key2]
                else:
                    if key2 not in columns[key1]:
                        columns[key1].append(key2)
    
    return columns

def getDict(df):
    dict1 = {}
    for key1, value1 in df.iterrows():
        for key2, value2 in value1.iteritems():
        # print key1, key2, value2
            if key1[0] not in dict1:
                dict1[key1[0]] = {key2:{key1[1]:value2}}
            elif key2 not in dict1[key1[0]]:
                dict1[key1[0]][key2] = {key1[1]:value2}
            else:
                dict1[key1[0]][key2][key1[1]] = value2
    
    return dict1

df = pd.read_pickle("dataframes/reviews3/fake/hierarchialIndexOfReviews.pkl")
df1 = pd.read_pickle("dataframes/reviews3/real/hierarchialIndexOfReviews.pkl")
# print df.head()
# print df1.head()

dict1 = getDict(df)
dict2 = getDict(df1)      

# print dict1
columns = {}
columns = getcolumns(dict1, columns)
columns = getcolumns(dict2, columns)

# print columns
dataFrames = {}
for key, value in dict1.iteritems():
    for key1, value1 in value.iteritems():
        dataFrames[key1] = df2 = pd.DataFrame(index=columns.get(key1), columns=['Real', 'Fake']).fillna(0)

for key, value in {"Real":dict2, "Fake":dict1}.iteritems():
    for key1, value1 in value.iteritems():
        for key2, value2 in value1.iteritems():
            for key3, value3 in value2.iteritems():
                # print key3, value3
                dataFrames.get(key2).loc[key3][key] = value3

for key , value in dataFrames.iteritems():
    #print value
    value.to_pickle('dataframes/reviews3/wordCatgry_wordVsCount/' + key + '_wordcat.pkl')
