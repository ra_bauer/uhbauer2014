import cPickle as pickle
from nltk.tag import map_tag
import pandas as pd
import numpy as np

frech_universal_co = {"NOUN":['NN', 'NNS', 'PRP', 'WP'], "VERB":['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ', 'MD', 'TO'],
                      "PRON":['NNP', 'NNPS'], "ADJ":['JJ', 'JJR', 'JJS'], "ADV":['RB', 'RBR', 'RBS'], "DET":['DT', 'WDT'],
                      "ADP":['IN'], "NUM":[], "CONJ":['CC'], "PRT":['RP']}

def getDict(path):
    posTagged = pickle.load(open(path, "rb"))
    simplifiedTagged = [(word, map_tag('en-ptb', 'universal', tag)) for word, tag in posTagged]  # print simplifiedTagged
    dict1 = {}
    for word in posTagged:
        if word[1] in dict1:
            dict1[word[1]] = dict1[word[1]] + 1
        else:
            dict1[word[1]] = 1
    return dict1

realDict = getDict("dataframes/reviews3/real/parts_of_speach.pickle")
fakeDict = getDict("dataframes/reviews3/fake/parts_of_speach.pickle")

df1 = pd.DataFrame(index=realDict.keys(), columns=['Real', 'Fake']).fillna(0)

for key, value in realDict.iteritems():
    df1['Real'].loc[key] = value
    
for key, value in fakeDict.iteritems():
    df1['Fake'].loc[key] = value
    
# print df1
sorting = []
df2 = np.round(df1.apply(lambda c: c / c.sum() * 100, axis=0), 2)
if df2['Real'].max() > df2['Fake'].max():
    sorting = ['Real']
else:
    sorting = ['Fake']
df3 = df2.sort(columns=sorting, ascending=False)
df3.to_pickle("dataframes/reviews3/parstOfSpeachTagging.pkl")
ax = df3.plot(kind='bar', fontsize=5)
for p in ax.patches:
        ax.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005), fontsize=5)
fig = ax.get_figure()
fig.savefig('plots/reviews3/partsOfSpeachPlot1.pdf')
print "Saved partsOfSpeachPlot1.pdf"
