'''
This script generates a dataframe, which contains count of each POS (present in lexicon)
'''

import pandas as pd

df = pd.read_pickle("lexiconAnalysis/data/lexicon.pkl")
df1 = pd.read_pickle("lexiconAnalysis/data/real/reviewsVsLexiconWords.pkl")

listOfWords = df1.columns.values
dict1 = {}

for key, value in df['PoS'].iteritems():
    if key in listOfWords:
        if value in dict1:
            dict1[value] = dict1.get(value) + 1
        else:
            dict1[value] = 1

pd.DataFrame.from_dict(dict1, orient="index").to_pickle("lexiconAnalysis/data/posVsWordCount.pkl")
