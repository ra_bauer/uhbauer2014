'''
This script generates a data frame, which contains the reviews number as index and words (present in lexicon) as column. Where the value is count of each word.
'''

import pandas as pd

df = pd.read_pickle("lexiconAnalysis/data/lexicon.pkl")
df1 = pd.read_pickle("liars4/data/fakeAbstract/filename_reviews_words.pkl")
# print df['word']
listofWords = df.index.values
listOfWordsPresent = []
dict1 = {}
for key, value in df1.iteritems():
    if key in listofWords:
        listOfWordsPresent.append(key)

# print listOfWordsPresent

df1.drop(list(set(list(df1.columns.values)) - set(listOfWordsPresent)),
         axis=1).to_pickle("lexiconAnalysis/data/fakeAbstract/reviewsVsLexiconWords.pkl")
