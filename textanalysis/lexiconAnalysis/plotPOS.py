'''
This script plots the pos data from the pkl generated from the previous script.
X axis: POS
Y Axis: Count
'''

import pandas as pd
import matplotlib.pyplot as plt


def plot1(df):
    df = df.head(35)
    ax = df.plot(kind='bar', fontsize=6)
    for p in ax.patches:
        ax.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005), fontsize=6)
    
    fig = ax.get_figure()
    fig.savefig('lexiconAnalysis/plots/' + 'posVsWordCount' + '.pdf')
    print "Saved "

def plotBarGraph(xaxis, yaxis, title, xlabel, ylabel):
    plt.bar(range(1, xaxis.__len__() + 1), yaxis, align='center')
    plt.xticks(range(1, xaxis.__len__() + 1), xaxis)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig('lexiconAnalysis/plots/' + title + '.pdf')

def getPos():
    pos = []
    f = open('lexiconAnalysis/resources/pos.txt', 'r')
    for line in f:
        pos.append(line.rstrip())
    return pos

df = pd.read_pickle("lexiconAnalysis/data/posVsWordCount.pkl")
df.columns = ['count']
df = df.sort(columns='count', ascending=False)

# plot all pos
plot1(df)

