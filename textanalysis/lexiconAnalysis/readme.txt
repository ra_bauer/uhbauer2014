Lexicon Analysis on liar4 data

lexicon.txt in resources, contains a set of words with POS. 
A data frame(lexicon.pkl) is generated from this text file and stored in data/ which is used in further analysis.

comments are present in individual source file (script) on how the script works and what the script is about.

The scripts are inter-dependent and should be run in an order.

1. readLexiconTxt.py
2. wordsInEachReview.py
3. posVsCountDF.py
4. plotPOS.py
5. other scripts are present as ipython files in ipython/ folder