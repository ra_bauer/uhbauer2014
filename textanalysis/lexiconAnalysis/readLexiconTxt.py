'''
This script generates lexicon.pkl from lexicon.txt
'''

import pandas as pd

df = pd.read_csv("lexiconAnalysis/resources/lexicon.txt", names=['wordID', 'word', 'lemma', 'PoS', '5', '6'], header=None, sep=r"\s+", index_col=1)
df1 = df[2:].drop('5', 1).drop('6', 1)
df1.to_pickle("lexiconAnalysis/data/lexicon.pkl")
