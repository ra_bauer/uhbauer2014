import pandas as pd
from utilities import Postgresql
from numpy import dtype
import sys
import os

# /mnt/data/ravi/stata/
path = "/mnt/data/ravi/stata/"

postgress = Postgresql()
con = postgress.connection_postgres()

def makeDir(tablename):
    os.mkdir(path + tablename)

def boolString(x):
    if x != None:
        return len(x) > 243
    else:
        return False
    
def writeToDta(table):
    #count = 20000
    flag = True
    increment = 10000
    counter = 0
    #while(flag and count > counter):
    while(flag):
        query = "select * from " + table + " LIMIT " + str(increment) + " OFFSET " + str(counter)
        # print query
        resultSet = postgress.executeSelectQueryPostGres(con, query);
        # print resultSet
        if(len(resultSet) > 0):
            df1 = pd.DataFrame(resultSet, index=range(len(resultSet)))
            dict1 = df1.columns.to_series().groupby(df1.dtypes).groups
            if dtype('<M8[ns]') in dict1:
                for column in dict1[dtype('<M8[ns]')]:
                    # print df1[column]
                    df1[column] = df1[column].map(lambda x: x.strftime('%Y.%m.%d') if not pd.isnull(x) else None)
                    # print df1[column]
            if dtype('O') in dict1:
                for column1 in dict1[dtype('O')]:
                    # print column1
                    df1[column1] = df1[column1].astype('str').map(lambda x: x[:242].decode("ascii", "ignore").encode() if boolString(x) else x.decode("ascii", "ignore").encode())
            # print "completed " + str(counter + len(resultSet)) + " records"
            try:
                df1.to_stata(path + table + "/" + table + "_" + str(counter) + ".dta")
            except:
                print "Unexpected error in saving file " + table + "_" + str(counter) + ".dta"   
            counter += increment
        else:
            flag = False
            print "Result set none"
    

# posts_stackoverflow
for arg in sys.argv[1:]:
    print "Table: " + arg
    makeDir(arg.strip())
    writeToDta(arg.strip())
