import os
from pandas.tools.merge import concat
import pandas as pd
import sys

path = "/mnt/data/ravi/stata/"

def removeDir(tablename):
    for root, dirs, files in os.walk(path + tablename, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))
    os.rmdir(path + tablename)
    
def mergeData(tablename):
    df = pd.DataFrame()
    for file1 in os.listdir(path + tablename):
        df1 = pd.read_stata(path + tablename + '/' + file1)
        df = concat([df, df1])
    df.to_stata(path + tablename + ".dta")
    
# posts_stackoverflow
for arg in sys.argv[1:]:
    print "folder: " + arg.strip()
    mergeData(arg.strip())
    removeDir(arg.strip())
