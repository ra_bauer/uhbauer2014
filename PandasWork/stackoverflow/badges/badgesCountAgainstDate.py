import psycopg2
import pandas as pd
import psycopg2.extras

def connection_postgres():
    conn_string = "host='localhost' port='5432' dbname='stackexchange' user='ravi' password='UHBauer@)!$'"
    print "Connecting to database\n    ->%s" % (conn_string)
    conn = psycopg2.connect(conn_string)
    print "Connected!\n"
    return conn

con = connection_postgres()
cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
try:
    cur.execute("select count(name),name,DATE(date) from badges_stackoverflow where name in (select name from valid_badges) group by DATE(date),name order by date,name")
except:
    print "query on badges_stackoverflow is wrong"
tests=cur.fetchall()
badges_dict={}
for test in tests:
    #print tests[i]['name'], tests[i]['count'], tests[i]['date']
    if test['date'] in badges_dict:
        badges_dict[test['date']][test['name']] = test['count']
    else:
        badges_dict[test['date']] = {test['name']:test['count']}
#print badges_dict
df = pd.DataFrame.from_dict(badges_dict,orient='index').fillna(0)
#print df
f = open('dataframe.html', 'w+')
f.write(df.to_html())

