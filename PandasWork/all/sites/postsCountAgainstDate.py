import psycopg2
import psycopg2.extras
import pandas as pd

def connection_postgres():
    conn_string = "host='localhost' port='5432' dbname='stackexchange' user='ravi' password='UHBauer@)!$'"
    print "Connecting to database\n    ->%s" % (conn_string)
    conn = psycopg2.connect(conn_string)
    print "Connected!\n"
    return conn

con = connection_postgres()
cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
try:
    cur.execute("select count(*) as count,date(creationdate) as date,sitename from (select id,sitename,creationdate,filepath from posts_meta union select id,sitename,creationdate,filepath from posts_superuser union select id,sitename,creationdate,filepath from posts_others union select id,sitename,creationdate,filepath from posts_serverfault union select id,sitename,creationdate,filepath from posts_stackoverflow ) A where sitename in (select name from valid_sites) group by date(creationdate),sitename order by date,count")
except:
    print "query on badges_stackoverflow is wrong"
tests=cur.fetchall()
site_dict={}
for test in tests:
    #print tests[i]['name'], tests[i]['count'], tests[i]['date']
    if test['sitename'] in site_dict:
        site_dict[test['sitename']][test['date']] = test['count']
    else:
        site_dict[test['sitename']] = {test['date']:test['count']}
print site_dict

for k, v in site_dict.iteritems():
    #print k, v
    df = pd.DataFrame.from_dict(v,orient='index').fillna(0)
    df.to_pickle("dataframes/postsAgainstDates/"+k+".pkl")