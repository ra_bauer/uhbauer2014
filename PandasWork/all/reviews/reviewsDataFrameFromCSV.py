# -*- coding: utf-8 -*-
import csv
import enchant
import pandas as pd
from collections import Counter
from nltk.tokenize import RegexpTokenizer
import nltk
from nltk.stem.snowball import SnowballStemmer

stemmer = SnowballStemmer("english")
stop = nltk.corpus.stopwords.words('english')
tokenizer = RegexpTokenizer(r'\w+')

def removeStopWords(line):
    return " ".join(word for word in line.split() if word not in stop)

def removeNonAsciiChars(line):
    return ''.join([i if ord(i) < 128 else '' for i in line])

def stemWords(line):
    return " ".join(stemmer.stem(word) for word in line.split())

def removeExclamations(line):
    return " ".join(tokenizer.tokenize(line))

listOfReviewNumbers = []
listOfUniqueWords = []
dict = {}
with open('resources/reviews_1.csv', 'rU') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', dialect='excel')
    for row in spamreader:
        dict[row[0]] = removeExclamations(stemWords(removeStopWords(removeNonAsciiChars(row[1]))))

d = enchant.Dict("en_US")

for reviewNumber, review in dict.iteritems():
    listOfReviewNumbers.append(reviewNumber)
    #print review
    for word in review.split():
        if d.check(word):
            if word not in listOfUniqueWords:
                listOfUniqueWords.append(word)

#print dict
#print listOfReviewNumbers
#print listOfUniqueWords

df = pd.DataFrame(index=listOfReviewNumbers, columns=listOfUniqueWords).fillna(0)
# df.to_pickle("dataframes/reviews/reviewNumberAgainstUniqueWordsEmpty.pkl")
for reviewNumber, review in dict.iteritems():
    for tuple in Counter(review.split()).most_common():
        df.loc[reviewNumber][tuple[0]] = tuple[1]

#print df
df.to_pickle("dataframes/reviews/reviewNumberAgainstUniqueWords.pkl")
df.to_csv("results/reviewsResults.csv")
