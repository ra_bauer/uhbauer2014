from nltk.stem.snowball import SnowballStemmer
import nltk
from nltk.tokenize import RegexpTokenizer

stemmer = SnowballStemmer("english")
stop = nltk.corpus.stopwords.words('english')
tokenizer = RegexpTokenizer(r'\w+')

def removeStopWords(line):
    return " ".join(word for word in line.split() if word not in stop)

def stemWords(line):
    return " ".join(stemmer.stem(word) for word in line.split())

def removeExclamations(line):
    return " ".join(tokenizer.tokenize(line))

print removeExclamations('Hello! world alkdjaslk ! (*& having')

