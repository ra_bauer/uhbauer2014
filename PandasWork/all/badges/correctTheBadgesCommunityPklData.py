import psycopg2
import psycopg2.extras
import pandas as pd

def connection_postgres():
    conn_string = "host='localhost' port='5432' dbname='stackexchange' user='ravi' password='UHBauer@)!$'"
    print "Connecting to database\n    ->%s" % (conn_string)
    conn = psycopg2.connect(conn_string)
    print "Connected!\n"
    return conn

def getAddedDataFrame(df, total, badge):
    try:
        total = total + df[badge]
        return total
    except:
        return total;   

con = connection_postgres()
cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
try:
    cur.execute("select * from valid_badges")
except:
    print "query is wrong"
tests = cur.fetchall()

badges_type = {}

for test in tests:
    if test['level'] in badges_type:
        badges_type[test['level']].append(test['name'])
    else:
        badges_type[test['level']] = [test['name']]
        
print badges_type

'''
badges_type = {}
badges_type['Moderation']=['Citizen Patrol']
badges_type['Moderation'].append('Deputy')
badges_type['Moderation'].append('Taxonomist')
badges_type['Question']=['Altruist']
badges_type['Question'].append('Benefactor')
badges_type['Answer']=['Enlightened']
badges_type['Answer'].append('Generalist')
print badges_type
'''

df1 = pd.read_pickle('dataframes/commmunity_badges.pkl').fillna(0)

total_moderation_badges = pd.DataFrame()
total_moderation_badges = df1['Marshal'] + df1['Deputy']
total_anwer_badges = pd.DataFrame()
total_anwer_badges = df1['Enlightened'] + df1['Generalist']
total_question_badges = pd.DataFrame()
total_question_badges = df1['Altruist'] + df1['Benefactor']
total_participation_badges = pd.DataFrame()
total_participation_badges = df1['Autobiographer'] + df1['Caucus']
total_other_badges = pd.DataFrame()
total_other_badges = df1['Analytical'] + df1['Announcer']

for key, value in badges_type.iteritems():
    for badge in value:
        if key == 'Moderation' and badge not in ['Marshal', 'Deputy']:
            print badge
            total_moderation_badges = getAddedDataFrame(df1, total_moderation_badges, badge)
        if key == 'Answer' and badge not in ['Enlightened', 'Generalist']:
            print badge
            total_anwer_badges = getAddedDataFrame(df1, total_anwer_badges, badge)
        if key == 'Question' and badge not in ['Altruist', 'Benefactor']:
            print badge
            total_question_badges = getAddedDataFrame(df1, total_question_badges, badge)
        if key == 'Participation' and badge not in ['Autobiographer', 'Caucus']:
            print badge
            total_participation_badges = getAddedDataFrame(df1, total_participation_badges, badge)
        if key == 'Others' and badge not in ['Analytical', 'Announcer']:
            print badge
            total_other_badges = total_other_badges + df1[badge]
print total_moderation_badges
print total_anwer_badges
print total_question_badges
print total_participation_badges
print total_other_badges

df1['total_moderation_badges_new'] = total_moderation_badges
df1['total_anwer_badges_new'] = total_anwer_badges
df1['total_question_badges_new'] = total_question_badges
df1['total_participation_badges_new'] = total_participation_badges
df1['total_other_badges_new'] = total_other_badges

df1.to_pickle("dataframes/community_badges_new.pkl")