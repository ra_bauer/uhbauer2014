import psycopg2
import psycopg2.extras
import pandas as pd

def connection_postgres():
    conn_string = "host='localhost' port='5432' dbname='stackexchange' user='ravi' password='UHBauer@)!$'"
    print "Connecting to database\n    ->%s" % (conn_string)
    conn = psycopg2.connect(conn_string)
    print "Connected!\n"
    return conn

con = connection_postgres()
cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
try:
    cur.execute("select count(B.type) as count,B.type,A.sitename,DATE(date) from (select name,date,sitename from badges_serverfault union all select name,date,sitename from badges_stackoverflow union all select name,date,sitename from badges_others union all select name,date,sitename from badges_superuser) A left outer join valid_badges B on A.name = B.name where B.level = 'Moderation' and A.sitename in ('stackoverflow.com','serverfault.com','superuser.com','android.com','apple.com','bicycles.com','cooking.com','english.com','gaming.com','math.com','photo.com') group by DATE(date),B.type,A.sitename order by date,B.type")
except:
    print "query is wrong"
tests = cur.fetchall()
badges_dict = {}

for test in tests:
    # print tests[i]['name'], tests[i]['count'], tests[i]['date']
    if test['sitename'] in badges_dict:
        if test['type'] in badges_dict[test['sitename']]:
            badges_dict[test['sitename']][test['type']][test['date']] = test['count']
        else:
            badges_dict[test['sitename']][test['type']] = {test['date']:test['count']}
    else:
        badges_dict[test['sitename']] = {test['type']:{test['date']:test['count']}}
        
## print badges_dict

for k, v in badges_dict.iteritems():
    for k1, v1 in v.iteritems():
        df = pd.DataFrame.from_dict(v1, orient='index').fillna(0)
        df.to_pickle("dataframes/moderationBadgesAgainstDate/" + k + "_" + k1 + ".pkl")
    
