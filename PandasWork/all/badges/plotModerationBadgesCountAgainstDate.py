'''
Created on Sep 5, 2014

@author: ravi
'''
from matplotlib.dates import date2num
import pickle
from matplotlib import pyplot as plt
from matplotlib.dates import AutoDateLocator
from matplotlib.dates import DateFormatter
import os

def create_plot(filePath, fileName):
    df = pickle.load(open(filePath, "rb"))
    x = []
    y = []
    for index, row in df.iterrows():
        x.append(date2num(index))
        y.append(row)
    
    fig = plt.figure()
    graph = fig.add_subplot(111)
    graph.plot(x, y)
    locator = AutoDateLocator()
    graph.xaxis.set_major_locator(locator)
    graph.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d'))
    graph.set_xlim(graph.get_xlim()[0] - 60, graph.get_xlim()[1] + 60)
    graph.set_ylim(graph.get_ylim()[0] - 1, graph.get_ylim()[1] + 1)
    fig.autofmt_xdate()
    fig.savefig('plots/moderationBadgesAgainstDate/' + fileName + '.pdf', format='pdf')

path = "dataframes/moderationBadgesAgainstDate";    
for file in os.listdir(path):
    if file.endswith(".pkl"):
        print 'creating plot for: ' + path + '/' + file
        create_plot(path + '/' + file, file)
        
