# -*- coding: utf-8 -*-

# Scrapy settings for MattressFirm project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'MattressFirm'

SPIDER_MODULES = ['MattressFirm.spiders']
NEWSPIDER_MODULE = 'MattressFirm.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'MattressFirm (+http://www.yourdomain.com)'
