# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class MattressfirmItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

class MattressFirmStateItem(scrapy.Item):
    state = scrapy.Field()
    link = scrapy.Field()

class MattressFirmCityItem(scrapy.Item):
    city = scrapy.Field()
    link = scrapy.Field()

class MattressFirmAddressItem(scrapy.Item):
    center = scrapy.Field()
    address = scrapy.Field()
    distnace = scrapy.Field()
    
class MattressFirmAddressItem1(scrapy.Item):
    center = scrapy.Field()
    address = scrapy.Field()
    distnace = scrapy.Field()
    
class MattressFirmAddressItem2(scrapy.Item):
    center = scrapy.Field()
    address = scrapy.Field()
    distnace = scrapy.Field()