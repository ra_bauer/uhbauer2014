import pymongo
import scrapy

url_prefix = "http://www.mystore411.com"
class StateSpider(scrapy.Spider):
    name = "statelocator"
    allowed_domains = ["mystore411.com/"]
    start_urls = [
        "http://www.mystore411.com/store/listing/2145/Mattress-Giant-store-locations",
    ]

    def parse(self, response):
        test = response.xpath('//table[@class="table1"]//tbody/tr/td/a')
        connection = pymongo.Connection()
        db = connection["mystore411"]
        states = db["states"]
        for sel in test:
            # print str(sel.xpath('text()').extract()).strip()
            if str(sel.xpath('text()').extract()).strip():
                # print str(sel.xpath('text()').extract()).strip()
                states.insert({"_id": str(sel.xpath('text()').extract()[0]).strip(), "url" : url_prefix + str(sel.xpath('@href').extract()[0])})
                print "saved a state into mongo: " + str(sel.xpath('text()').extract()[0]).strip()