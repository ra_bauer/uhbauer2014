import pymongo
import scrapy
import time

url_prefix = "http://www.mystore411.com"
connection = pymongo.Connection()
db = connection["mystore411"]

class AddressSpider1(scrapy.Spider):
    name = "addresslocator1"
    allowed_domains = ["mystore411.com"]
    cities = db["cities"]
    start_urls = []
    cursor = db.cities.find()
    for city in cursor:
        # print state.get("_id")
        #print "Appending url: " + city.get("url")
        start_urls.append(city.get("url"));

    def parse(self, response):
        dict1 = {}
        try:
            dict1 = {"type":"found"}
            response = response.replace(body=response.body.replace('<br />', '\n')) 
            test = response.xpath('//div[@class="store-details"]/p/text()')
            dict1.update({"location" :' '.join(test.extract()[0].encode('utf-8').split())})
            test1 = response.xpath('//h1[@class="store_title"]/text()')
            dict1.update({"storeTitile":' '.join(test1.extract()[0].encode('utf-8').split())});
        except Exception:
            dict1 = {"type":"notfound"}
            test = response.xpath('//table[@class="table1"]//tbody/tr/td/a/@href')
            print "Not found: " , url_prefix + ' '.join(test.extract()[0].encode('utf-8').split())
            dict1.update({"url": url_prefix + ' '.join(test.extract()[0].encode('utf-8').split())})
        print "saved address into mongo"
        centreAddresses = db["centreAddresses"]
        centreAddresses.insert(dict1)
        time.sleep(10)
        
        
