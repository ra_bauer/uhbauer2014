import pymongo
import scrapy

url_prefix = "http://www.mattressfirm.com"
connection = pymongo.Connection()
db = connection["MattressFirm"]

class CentreSpider(scrapy.Spider):
    name = "centerlocator"
    allowed_domains = ["mattressfirm.com"]
    cities = db["cities"]
    start_urls = []
    cursor = db.cities.find()
    for city in cursor:
        # print state.get("_id")
        # print "Appending url: " + state.get("url")
        start_urls.append(city.get("url"));

    def parse(self, response):
        response = response.replace(body=response.body.replace('<br />', '\n')) 
        test = response.xpath('//table[@class="resultsListing"]//tr[@class="unselectedLoc"]//td/a')
        centers = db["centers"]
        for centre in test:
            centers.insert({"_id" : ' '.join(str(centre.xpath('text()').extract()[0]).split()) , "url" : url_prefix + str(centre.xpath('@href').extract()[0])})
            print "Inserting centre into mongo: " + ' '.join(str(centre.xpath('text()').extract()[0]).split())
