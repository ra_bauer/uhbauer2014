import pymongo
import scrapy

url_prefix = "http://www.mattressfirm.com"
connection = pymongo.Connection()
db = connection["MattressFirm"]

class AddressSpider(scrapy.Spider):
    name = "addresslocator"
    allowed_domains = ["mattressfirm.com"]
    centers = db["centers"]
    start_urls = []
    cursor = db.centers.find()
    for center in cursor:
        # print state.get("_id")
        # print "Appending url: " + state.get("url")
        start_urls.append(center.get("url"));

    def parse(self, response):
        dict = {}
        response = response.replace(body=response.body.replace('<br />', '\n')) 
        test = response.xpath('//span[@id="LabelStoreID"]/text()')
        dict.update({"location" :' '.join(test.extract()[0].encode('utf-8').split())})
        test = response.xpath('//span[@id="LabelAddressLine2"]/text()')
        dict.update({"location1":' '.join(test.extract()[0].encode('utf-8').split())})
        test = response.xpath('//span[@id="LabelDirections"]/text()')
        dict.update({"address1":' '.join(test.extract()[0].encode('utf-8').split())})
        test = response.xpath('//span[@id="LabelAddressLine1"]/text()')
        dict.update({"address2":' '.join(test.extract()[0].encode('utf-8').split())})
        test = response.xpath('//span[@id="LabelCityStateZip"]/text()')
        dict.update({"city&zip":' '.join(test.extract()[0].encode('utf-8').split())})
        
        centreAddresses = db["centreAddresses"]
        centreAddresses.insert(dict)
        
        