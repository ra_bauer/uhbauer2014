import pymongo
import scrapy
import time

url_prefix = "http://www.mystore411.com"
connection = pymongo.Connection()
db = connection["mystore411"]
class CitySpider(scrapy.Spider):
    name = "citylocator"
    allowed_domains = ["mystore411.com"]
    states = db["states"]
    start_urls = []
    cursor = db.states.find()
    for state in cursor:
        # print state.get("_id")
        # print "Appending url: " + state.get("url")
        start_urls.append(state.get("url"));

    def parse(self, response):
        test = response.xpath('//table[@class="table1"]//tbody/tr/td/a')
        print test
        cities = db["cities"]
        for sel in test:
            print str(sel.xpath('text()').extract()).strip()
            time.sleep(1)
            if len(sel.xpath('text()').extract()) > 0:
                cities.insert({"_id": str(sel.xpath('text()').extract()[0]).strip(), "url" : url_prefix + str(sel.xpath('@href').extract()[0])})
                print "saved a city into mongo: " + str(sel.xpath('text()').extract()[0]).strip()
