import pymongo
import scrapy
import time

url_prefix = "http://www.mystore411.com"
connection = pymongo.Connection()
db = connection["mystore411"]

class AddressSpider2(scrapy.Spider):
    name = "extraaddloc"
    allowed_domains = ["mystore411.com"]
    centreAddresses = db["centreAddresses"]
    start_urls = []
    cursor = db.centreAddresses.find({"type":"notfound"})
    for centre in cursor:
        # print "Appending url: " + city.get("url")
        start_urls.append(centre.get("url"));
    
    def parse(self, response):
        dict1 = {}
        try:
            dict1 = {"type":"found"}
            response = response.replace(body=response.body.replace('<br />', '\n')) 
            test = response.xpath('//div[@class="store-details"]/p/text()')
            dict1.update({"location" :' '.join(test.extract()[0].encode('utf-8').split())})
            test1 = response.xpath('//h1[@class="store_title"]/text()')
            dict1.update({"storeTitile":' '.join(test1.extract()[0].encode('utf-8').split())});
            print "saved address into mongo"
            centreAddresses = db["centreAddresses"]
            centreAddresses.insert(dict1)
            time.sleep(10)
        except Exception:
            pass
        
        
