from geopy.geocoders import Nominatim
import csv
import pymongo
from geopy.exc import GeocoderTimedOut

array1 = []
geolocator = Nominatim()

connection = pymongo.Connection()
db = connection["mystore411"]
centreAddresses = db["centreAddresses"]
cursor = db.centreAddresses.find({"type":"found"})
for centre in cursor:
    try:
        dict1 = {}
        dict1['storeTitile'] = centre.get('storeTitile')
        dict1['address'] = centre.get('location')
        # print centre.get('location')
        location = geolocator.geocode(centre.get('location'))
        # print location
        if location is not None:
            dict1['lat'] = location.latitude
            dict1['long'] = location.longitude
        else:
            dict1['lat'] = ""
            dict1['long'] = ""
        print dict1
        array1.append(dict1)
    except GeocoderTimedOut:
        pass

print array1    
with open('locations_mystore411.csv', 'w') as csvfile:
    fieldnames = ['storeTitile', 'address', 'lat', 'long']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for row in array1:
        writer.writerow(row)
