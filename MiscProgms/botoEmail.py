from boto.mturk.connection import MTurkConnection

access_key = '[your access key]'
secret_access_key = '[your secret access key]'

c = MTurkConnection (access_key, secret_access_key)
c.get_account_balance()

subject = "whatever you want the subject of the email to workers to be"

message = "whatever you want the message text to be."

workerids = ["A360SF8N3MPFDV", "AH78W67KLW0IH1"]

for workedId in workerids:
    c.notify_workers(workedId, subject, message)
    
